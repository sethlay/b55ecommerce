<!DOCTYPE html>
<html>
<head>
	<title><?php get_title(); ?> Restie-Exzec</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/united/bootstrap.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	<header>
	<!-- Navbar from Bootswatch -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Restie-Exzec</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarColor02">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
		       			<a class="nav-link" href="catalog.php">Exzecs Menu <span class="sr-only">(current)</span></a>
		      		</li>
		      		<?php  
		      			session_start();
		      			if(isset($_SESSION['user']) && $_SESSION['user']['role_id']==1){
		      		?>
		      		<li class="nav-item">
		        		<a class="nav-link" href="add-item.php">Add Item</a>
		      		</li>
		      		<li class="nav-item">
		        		<a class="nav-link" href="">Transaction</a>
		      		</li>
		      		<?php  
		      			}else{
					?>

		      			<li class="nav-item">
		      				<a href="../views/profile.php" class="nav-link">Profile</a>
		      			</li>
		      			<li class="nav-item">
		      				<a href="../views/history.php" class="nav-link">History</a>
		      			</li>
			      		<li class="nav-item">
			      			<a class="nav-link" href="cart.php">Cart <span class="badge bg-warning" id="cartCount">
			      				<?php 
			      					if(isset($_SESSION['cart'])){
			      						echo array_sum($_SESSION['cart']);
			      					}else{
			      						echo 0;			      				 
			      				 	}
			      				 ?>	
			      				</span>
			      			</a>		      				
			      		</li>
					<?php		      				
		      			}

		      			if(isset($_SESSION['user'])){
		      		?>
		      			<li class="nav-item">
		      				<a class="nav-link" href="">Hello <?= $_SESSION['user']['firstName']  ?>!</a>
		      			</li>
		      			<li class="nav-item">
		      				<a href="../controllers/logout-process.php" class="nav-link">Logout</a>
		      			</li>
		      		<?php
		      			}else{
		      		?>
		      			<li class="nav-item">
			        		<a class="nav-link" href="login.php">Login</a>
			      		</li>
			      		<li class="nav-item">
			        		<a class="nav-link" href="register.php">Register</a>
			      		</li>
		      		<?php		
		      			}
		      		?>
		      		
		    	</ul>
		  	</div>
		</nav>
		<!-- Page Contents -->
		<?php get_body_contents() ?>
	</header>
		<!-- Footer -->
	<footer class="page-footer font-small navbar-danger bg-light">
		<div class="footer-copyright text-center py-3">© B55 Exzecs By: Truly Yours</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>